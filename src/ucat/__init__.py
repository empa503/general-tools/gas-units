from fnmatch import fnmatch
from typing import Union, Tuple
import warnings
import re

import numpy as np
import scipy.constants

try:
    import xarray
except ModuleNotFoundError:
    xarray = None

__version__ = "0.2.0"

N_A = scipy.constants.N_A
R = scipy.constants.R
g = scipy.constants.g
p0 = scipy.constants.physical_constants["standard atmosphere"][0]  # pressure, unit: Pa
T0 = 288.15  # temperature, unit: K

METRIC_PREFIXES = {
    "Y": 1e24,  # yotta
    "Z": 1e21,  # zetta
    "E": 1e18,  # exa
    "P": 1e15,  # peta
    "T": 1e12,  # tera
    "G": 1e9,  # giga
    "M": 1e6,  # mega
    "k": 1e3,  # kilo
    "h": 1e2,  # hecto
    "da": 1e1,  # deca
    "": 1e0,
    "d": 1e-1,  # deci
    "c": 1e-2,  # centi
    "m": 1e-3,  # milli
    "u": 1e-6,  # micro
    "µ": 1e-6,  # micro (again)
    "n": 1e-9,  # nano
    "p": 1e-12,  # pico
    "f": 1e-15,  # femto
    "a": 1e-18,  # atto
    "z": 1e-21,  # zepto
    "y": 1e-24,  # yocto
}


# molar mass (kg/m3)
M = {
    "AIR": 28.97e-3,  # dry air
    "CO": 28.01e-3,
    "CO2": 44.01e-3,
    "14CO2": 45.99307e-3,
    "H2O": 18.01528e-3,
    "NO2": 46.0055e-3,
    "NO": 30.01e-3,
    "O3": 48.00e-3,
    "SO2": 64.066e-3,
    "CH4": 16.0425e-3,
}

# time unit in seconds
time_unit_base = {
    "s": 1,
    "min": 60,
    "h": 60 * 60,
    "day": 60 * 60 * 24,
    "week": 60 * 60 * 24 * 7,
    "month": (365 * 3 + 366) / (4 * 12) * 60 * 60 * 24,
    "year": (365 * 3 + 366) / 4 * 60 * 60 * 24,
}

# include aliases for "year" and "day"
TIME_UNITS = {
    **time_unit_base,
    **{alias: time_unit_base["year"] for alias in ["yr", "y", "a"]},
    **{alias: time_unit_base["day"] for alias in ["d"]},
    **{alias: time_unit_base["h"] for alias in ["hr"]},
}


def _unit2quantity(unit: str) -> Tuple[str, float, str]:
    """
    Get quantitity from unit and conversion factor to convert unit
    to SI units.

    Quantities (point | column):
        xM: mole/molar fraction  | column-averaged dry air mole fraction
        xV: volume mixing ratios | column-averaged dry air volume mixing ratio
        xm: mass mixing ratios   | column-averaged dry air mass mixing ratio
        cM: molar concentration  | molar column density
        cn: number density       | number column density
        cm: mass concentration   | mass column density
    """
    unit = unit.strip()

    quantity = None
    conv = 1.0
    point_or_column = "unknown"

    if " " in unit or "/" in unit:
        n, d = parse_unit(unit)

        # molar or mass concentration or volume mixing ratio
        if d.endswith("m3") or d.endswith("m2"):

            point_or_column = "point" if d.endswith("m3") else "column"

            conv /= METRIC_PREFIXES[d[:-2]] ** int(d[-1])

            if n.endswith("mol"):
                quantity = "cM"
                conv *= METRIC_PREFIXES[n[:-3]]

            elif n.endswith("g"):
                quantity = "cm"
                conv *= 1e-3 * METRIC_PREFIXES[n[:-1]]

            elif n.endswith("m3"):
                quantity = "xV"
                conv *= METRIC_PREFIXES[n[:-2]]

        # mass mixing ratio
        elif n.endswith("g") and d.endswith("g"):
            quantity = "xm"
            conv *= 1e-3 * METRIC_PREFIXES[n[:-1]]
            conv /= 1e-3 * METRIC_PREFIXES[d[:-1]]

        # molar fraction
        elif n.endswith("mol") and d.endswith("mol"):
            quantity = "xM"
            conv *= METRIC_PREFIXES[n[:-3]]
            conv /= METRIC_PREFIXES[d[:-3]]

    else:
        # molar fraction
        if fnmatch(unit, "pp?v"):
            quantity = "xM"
            conv *= {"m": 1e-6, "b": 1e-9, "t": 1e-12}[unit[2]]

        # mass mixing ratio
        elif fnmatch(unit, "pp?m"):
            quantity = "xm"
            conv *= {"m": 1e-6, "b": 1e-9, "t": 1e-12}[unit[2]]

        # number density
        elif unit.endswith("m-3"):
            point_or_column = "point"
            quantity = "cn"
            conv /= METRIC_PREFIXES[unit[:-3]] ** 3

        elif unit.endswith("m-2"):
            point_or_column = "column"
            quantity = "cn"
            conv /= METRIC_PREFIXES[unit[:-3]] ** 2

    if quantity is None:
        raise ValueError('Failed to parse unit "%s"' % unit)

    return quantity, conv, point_or_column


def parse_unit(unit: str) -> Tuple[str, str]:
    if "/" in unit:
        n, d = map(str.strip, unit.split("/"))
    elif " " in unit:
        n, d = map(str.strip, unit.split())
    else:
        raise ValueError(f"{unit} cannot be parsed.")

    # remove "-" and "1"
    d = d.replace("-", "")
    d = d.replace("1", "")

    return n, d


def convert_points(
    x: Union[int, float, np.ndarray, xarray.DataArray],
    from_unit: str,
    to_unit: str,
    molar_mass: Union[str, int, float] = None,
    p: Union[int, float] = p0,
    T: Union[int, float] = T0,
    warn: bool = True,
) -> Union[int, float, np.ndarray, xarray.DataArray]:
    """
    Convert between mole fractions, mixing rations and different
    concentrations.

    The library is still work in progress. Please check if your results
    reasonable and let us know, if you find a bug.

    Parameters:
        x:          value that needs to be converted (numerical)
        from_unit: unit of `x` (e.g. "mol mol-1", "ug/m3", ...)
        to_unit:   unit to which `x` will be converted.
        molar_mass: name or molar mass value of gas
    """

    from_, from_conversion, from_point_or_column = _unit2quantity(from_unit)
    to, to_conversion, to_point_or_column = _unit2quantity(to_unit)

    if warn and (from_point_or_column == "column" or to_point_or_column == "column"):
        warnings.warn(
            "Given units indicate column quantities but this function is for converting to point quantities."
        )

    if isinstance(molar_mass, str):
        Mi = M[molar_mass.upper()]
    else:
        Mi = molar_mass

    if Mi is None and (
        (from_ in ["xm", "cm"] and to not in ["xm", "cm"])
        or (from_ not in ["xm", "cm"] and to in ["xm", "cm"])
    ):
        raise ValueError(
            'Need molar mass to convert %s to %s but M is "%s"' % (from_, to, Mi)
        )

    # convert to molar fraction (in mol mol-1)
    if from_ in ["xM", "xV"]:
        pass
    elif from_ == "xm":
        x = x * M["AIR"] / Mi
    elif from_ == "cM":
        x = x * R * T / p
    elif from_ == "cn":
        x = x / N_A * R * T / p
    elif from_ == "cm":
        x = x / Mi * R * T / p
    else:
        raise ValueError('Cannot convert from "%s"' % from_)

    # convert mole fraction to output unit
    if to in ["xM", "xV"]:
        pass
    elif to == "xm":
        x = x * Mi / M["AIR"]
    elif to == "cM":
        x = x * p / (R * T)
    elif to == "cn":
        x = x * N_A * p / (R * T)
    elif to == "cm":
        x = x * Mi * p / (R * T)
    else:
        raise ValueError('Cannot convert to "%s"' % to)

    x_converted = x * from_conversion / to_conversion

    if xarray is not None and isinstance(x, xarray.DataArray):
        _check_xarray_unit(x_converted, from_unit)
        x_converted.attrs["units"] = to_unit
        return x_converted

    return x_converted


def convert_columns(
    x: Union[int, float, np.ndarray, xarray.DataArray],
    from_unit: str,
    to_unit: str,
    molar_mass: Union[str, int, float] = None,
    p_surface: Union[int, float] = p0,
    T_surface: Union[int, float] = T0,
    warn: bool = True,
) -> Union[int, float, np.ndarray, xarray.DataArray]:
    """
    Convert between vertical column densities and column-averaged
    dry air mole fractions.

    The library is still work in progress. Please check if your results
    reasonable and let us know, if you find a bug.

    Parameters:
        x:          value that needs to be converted (numerical)
        from_unit: unit of `x` (e.g. "mol mol-1", "ug/m3", ...)
        to_unit:   unit to which `x` will be converted.
        molar_mass: name or molar mass value of gas
        p_surface:  surface pressure (in Pa)
    """
    # handle path integrated mixing ratios differently
    if any((_detect_ppm_m(from_unit), _detect_ppm_m(to_unit))):
        from_unit_vol = col_unit2vol_unit(from_unit)
        to_unit_vol = col_unit2vol_unit(to_unit)
        
        x_converted = convert_points(
            x, from_unit_vol, to_unit_vol, molar_mass, p_surface, T_surface, warn
        )
        if xarray is not None and isinstance(x, xarray.DataArray):
            x_converted.attrs["units"] = to_unit

        return x_converted

    from_, from_conversion, from_point_or_column = _unit2quantity(from_unit)
    to, to_conversion, to_point_or_column = _unit2quantity(to_unit)

    if warn and (from_point_or_column == "point" or to_point_or_column == "point"):
        warnings.warn(
            "Given units indicate point quantities but this function is for converting to column quantities."
        )

    # molar mass
    if isinstance(molar_mass, str):
        Mi = M[molar_mass.upper()]
    else:
        Mi = molar_mass

    if Mi is None and (
        (from_ in ["xm", "cm"] and to not in ["xm", "cm"])
        or (from_ not in ["xm", "cm"] and to in ["xm", "cm"])
    ):
        raise ValueError(
            'Need molar mass to convert %s to %s but M is "%s"' % (from_, to, Mi)
        )

    air_column = surface_pressure_to_airnumber(p_surface)

    # convert to column density (in mol m-2)
    if from_ == "cM":
        pass
    elif from_ == "cn":
        x = x / N_A
    elif from_ == "cm":
        x = x / Mi
    elif from_ == "xM":
        x = x * air_column
    elif from_ == "xn":
        x = x * air_column / N_A
    elif from_ == "xm":
        x = x * air_column / Mi

    # convert molar column density to output unit
    if to == "cM":
        pass
    elif to == "cn":
        x = x * N_A
    elif to == "cm":
        x = x * Mi
    elif to == "xM":
        x = x / air_column
    elif to == "xn":
        x = x / air_column
    elif to == "xm":
        x = x / air_column * Mi / M["AIR"]

    x_converted = x * from_conversion / to_conversion

    if xarray is not None and isinstance(x, xarray.DataArray):
        _check_xarray_unit(x_converted, from_unit)
        x_converted.attrs["units"] = to_unit
        return x_converted

    return x_converted


def _detect_ppm_m(unit: str) -> bool:
    """Detect the unit ppm m to prevent it from being recognised as nominator and denominator"""
    pattern = r"^pp[mbt][mv]?( |-)m$"

    return bool(re.match(pattern, unit))


def _check_xarray_unit(data: xarray.DataArray, from_unit: str):
    """Check if the unit in the xarray DataArray match the given from_unit.

    Args:
        data (xr.DataArray):
            Data for which the unit are to be converted.
        from_unit (str):
            Unit to which the data is converted.

    Raises:
        ValueError: Raise error if the units do not match
    """
    actual_unit = data.attrs.get("units", None)

    if actual_unit is not None:
        actual_unit = format_unit(actual_unit, sep="/")
        from_unit = format_unit(from_unit, sep="/")
        if actual_unit != from_unit:
            raise ValueError(
                f"Units mismatch: Expected '{from_unit}', found '{actual_unit}'."
            )


def format_unit(unit: str, sep: str) -> str:
    """Format unit to have a consistent notation,
    e.g. m s-1, kg s-1 etc.

    Args:
        to_unit (str):
            Unit which should be formatted
        sep (str):
            Separator beteen nominator and denominator.
            Recommended: ' ' or '/'

    Returns:
        str: Formatted unit
    """
    if _detect_ppm_m(unit):
        return unit
    try:
        n, d = parse_unit(unit)
    except ValueError:
        return unit

    if "/" in sep:
        formatted_unit = f"{n}{sep}{d}"
    else:
        if d[-1].isdigit():
            d = f"{d[:-1]}-{d[-1]}"
        else:
            d = f"{d}{-1}"

        formatted_unit = f"{n}{sep}{d}"

    return formatted_unit


def col_unit2vol_unit(col_unit: str) -> str:
    """Determine the corresponding volumetric unit if a column unit is given.

    Args:
        col_unit (str): Target unit of the column.

    Raises:
        ValueError: Raise error if the unit could not be parsed.

    Returns:
        str: Corresponding volumetric unit
    """

    if _detect_ppm_m(col_unit):
        col_unit_clean = col_unit[:4].strip(" ").strip("-")

        if len(col_unit_clean) == 3:
            col_unit_clean += "v"
        elif len(col_unit_clean) and (col_unit_clean[-1] in ["m", "v"]):
            pass
        else:
            raise ValueError(
                f"{col_unit_clean } is not supported. Expected 'm' or 'v' suffix."
            )
        return col_unit_clean

    n, d = parse_unit(col_unit)

    if d.endswith("m2"):
        return f"{n}/{d.replace('m2', 'm3')}"
    elif d.endswith("m3"):
        return f"{n}/{d}"
    else:
        raise ValueError(f"{col_unit} is not supported.")


def surface_pressure_to_airnumber(
    p_surf: Union[int, float, np.ndarray]
) -> Union[int, float, np.ndarray]:
    """Convert surface pressure (Pa) to mol of air molecules (mol m-2)."""

    return p_surf / g / M["AIR"]


def convert_mass_per_time_unit(
    x: Union[int, float, np.ndarray, xarray.DataArray], from_unit: str, to_unit: str
) -> Union[int, float, np.ndarray, xarray.DataArray]:
    # parse unit
    from_mass_unit, from_time_unit = parse_unit(from_unit)
    to_mass_unit, to_time_unit = parse_unit(to_unit)

    # calculate mass conversion factors
    from_mass_conversion = _mass_conversion_factor(from_mass_unit)
    to_mass_conversion = _mass_conversion_factor(to_mass_unit)

    # calculate conversion factor
    conversion_factor = (from_mass_conversion / to_mass_conversion) * (
        TIME_UNITS[to_time_unit] / TIME_UNITS[from_time_unit]
    )

    x_converted = x * conversion_factor

    if xarray is not None and isinstance(x, xarray.DataArray):
        _check_xarray_unit(x_converted, from_unit)
        x_converted.attrs["units"] = to_unit
        return x_converted

    return x_converted


def _mass_conversion_factor(mass_unit: str) -> float:
    "Calculate the conversion factor for a unit of mass to g"
    if mass_unit.endswith("t"):
        mass_conv = 1e6
    elif mass_unit.endswith("g"):
        mass_conv = 1
    else:
        raise ValueError(f"Failed to parse unit {mass_unit}")
    prefix = mass_unit[:-1]
    return mass_conv * METRIC_PREFIXES.get(prefix, 1)
